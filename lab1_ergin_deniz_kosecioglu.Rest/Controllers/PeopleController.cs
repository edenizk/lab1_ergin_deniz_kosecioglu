using lab1_ergin_deniz_kosecioglu.Rest.Context;
using lab1_ergin_deniz_kosecioglu.Rest.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;


namespace Lab1_aziemianski.Rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly AzureDbContext context;

        public PeopleController(AzureDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Person>> Get()
        {
            var people = context.People.ToList();
            return people;
        }

        [HttpGet("{id}")]
        public ActionResult<Person> Get(int id)
        {
            //TODO: get person from database
            var person = context.People.Find(id);
            if(person == null)
            {
                return NotFound();
            }
            return person;
        }

        [HttpPost]
        public HttpResponseMessage Post([FromBody] Person value)
        {
            if (value.FirstName != null && value.LastName != null)
            {
                context.People.Add(value);
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.Created);
            }

            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        [HttpPut("{id}")]
        public HttpResponseMessage Put(int id, [FromBody] Person value)
        {
            var person = context.People.Find(id);

            if (person != null && value != null)
            {
                person.FirstName = value.FirstName != null ? value.FirstName : person.FirstName;
                person.LastName = value.LastName != null ? value.LastName : person.LastName;
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.NotModified);
        }


        [HttpDelete("{id}")]
        public HttpResponseMessage Delete(int id)
        {
            //TODO: Remove person from database
            var person = context.People.Find(id);
            if(person != null)
            {
                context.People.Remove(person);
                context.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }

            return new HttpResponseMessage(HttpStatusCode.NotModified);
        }
    }
}