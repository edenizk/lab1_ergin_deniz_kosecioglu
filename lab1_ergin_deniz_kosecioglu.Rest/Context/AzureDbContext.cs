using lab1_ergin_deniz_kosecioglu.Rest.Models;
using Microsoft.EntityFrameworkCore;

namespace lab1_ergin_deniz_kosecioglu.Rest.Context
{
    public class AzureDbContext : DbContext
    {
        public AzureDbContext(DbContextOptions<AzureDbContext> options)
            : base(options)
        {
        }

        protected AzureDbContext()
        {
        }

        public DbSet<Person> People { get; set; }
    }
}